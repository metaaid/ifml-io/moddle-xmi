import expect from '../expect.js';


describe('moddle-id', function() {

  it('should expose CJS bundle', async function() {
    const mod = await import('../../dist/index.cjs');
    expect(mod.Reader).to.exist;
    expect(mod.Writer).to.exist;
  });


  it('should expose UMD bundle', async function() {
    const mod = await import('../../dist/moddle-xmi.umd.cjs');
    expect(mod.Reader).to.exist;
    expect(mod.Writer).to.exist;
  });

});
