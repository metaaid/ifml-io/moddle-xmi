import expect from '../../expect.js';

import {
  Writer
} from '../../../lib/index.js';

import {
  createXMIModelBuilder,
  createModelBuilder,
} from '../../helper.js';

import {
  assign
} from 'min-dash';


function xmiWrap(model, childs, rootOptions){
  var root = model.create("xmi:XMI", rootOptions);
  var ownedElement = root.get("ownedElement");
  ownedElement.push(childs);
  return root;
}

function xmiCreateRoot(model, firstChildsName, options = {}, rootOptions = {}){
  var dcRoot = model.create(firstChildsName, options);
  return xmiWrap(model, dcRoot, rootOptions);
}

describe('XMI-Writer', function() {

  var createModel = createXMIModelBuilder('test/fixtures/model/');

  function createWriter(model, options) {
    return new Writer(assign({ preamble: false }, options || {}));
  }


  describe('should export', function() {
    

    describe('base', function() {

      var model = createModel([ 'properties' ]);

      it('should write xml preamble', function() {

        // given
        var writer = new Writer({ preamble: true });
        var root = model.create('xmi:XMI');

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<?xml version="1.0" encoding="UTF-8"?>\n' +
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" />');
      });
    });
    


    describe('datatypes', function() {

      var datatypesModel = createModel(['datatype', 'datatype-external', 'datatype-aliased']);

      describe('xmi', function() {
        it('as default namespace', function() {

          // given
          var writer = createWriter(datatypesModel);
          
          var root = datatypesModel.create('xmi:XMI', {
            'xmlns': 'http://www.omg.org/spec/XMI/20131001',
          });
          var error = null;
  
          // when
          try {
            writer.toXML(root);
          } catch(err){
            error = err;
          }

          expect(error).to.exist;
          expect(error.message).to.contain("XMI namespace beeing the default namespace is not supported")
        });
      });
      
      it('via xsi:type', function() {

        // given
        var writer = createWriter(datatypesModel);


        var root = xmiCreateRoot(datatypesModel, 'dt:Root', {
          // y: 100,
          'bounds': datatypesModel.create('dt:Rect', {
            y: 100 
          })
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:dt="http://datatypes">' +
            '<dt:root>' +
              '<bounds xmi:type="dt:Rect" y="100" />' +
            '</dt:root>' +
          '</xmi:XMI>'
        );
      });


      it('via xsi:type / default / extension attributes', function() {

        // given
        var writer = createWriter(datatypesModel);

        var root = xmiCreateRoot(datatypesModel, 'dt:Root', {
          'bounds': datatypesModel.create('xmi:XMI', { 
            y: 100,
            'xmlns:f': 'http://foo',
            'f:bar': 'BAR'
          })
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:dt="http://datatypes">' +
            '<dt:root>' +
              '<bounds xmlns:f="http://foo" xmi:type="xmi:XMI" y="100" f:bar="BAR" />' +
            '</dt:root>' +
          '</xmi:XMI>');
      });


      it('via xsi:type / explicit / extension attributes', function() {

        // given
        var writer = createWriter(datatypesModel);

        var root = xmiCreateRoot(datatypesModel, 'dt:Root', {
          'bounds': datatypesModel.create('do:Rect', {
            x: 100,
            'xmlns:f': 'http://foo',
            'f:bar': 'BAR'
          })
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:dt="http://datatypes">' +
          '<dt:root>' +
            '<bounds xmlns:do="http://datatypes2" ' +
                      'xmlns:f="http://foo" ' +
                      'xmi:type="do:Rect" ' +
                       'x="100" f:bar="BAR" />' +
          '</dt:root>' +
          '</xmi:XMI>'
        );
      });


      it('via xsi:type / no namespace', function() {

        // given
        var writer = createWriter(datatypesModel);

        var root = xmiCreateRoot(datatypesModel, 'dt:Root', { 
          xmlns: 'http://datatypes' ,
          'bounds': datatypesModel.create('dt:Rect', { y: 100 })
        });


        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
          '<root xmlns="http://datatypes">' +
            '<bounds xmi:type="Rect" y="100" />' +
          '</root>' +
          '</xmi:XMI>'
          );
      });


      it('via xsi:type / other namespace', function() {

        // given
        var writer = createWriter(datatypesModel);

        var root = xmiCreateRoot(datatypesModel, 'dt:Root', {
          'xmlns:a' : 'http://datatypes' ,
          'bounds': datatypesModel.create('dt:Rect', { y: 100 })
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
          '<a:root xmlns:a=\"http://datatypes\">' +
            '<bounds xmi:type="a:Rect" y="100" />' +
          '</a:root>' +
          '</xmi:XMI>');
      });


      it('via xsi:type / in collection / other namespace)', function() {

        // given
        var writer = createWriter(datatypesModel);

        var root = xmiCreateRoot(datatypesModel, 'dt:Root', {
          'otherBounds': [
            datatypesModel.create('dt:Rect', { y: 200 }),
            datatypesModel.create('do:Rect', { x: 100 }),
          ]
        });

        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:dt="http://datatypes" xmlns:do="http://datatypes2">' +
          '<dt:root>' +
                  //  'xmlns:do="http://datatypes2" ' +
                  //  'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<otherBounds xmi:type="dt:Rect" y="200" />' +
            '<otherBounds xmi:type="do:Rect" x="100" />' +
          '</dt:root>' +
          '</xmi:XMI>');
      });

      it('via xsi:type / in collection / type prefix', function() {

        // given
        var writer = createWriter(datatypesModel);
        var root = xmiCreateRoot(datatypesModel, 'dt:Root', {
          'otherBounds': [
            datatypesModel.create('da:Rect', { z: 200 }),
            datatypesModel.create('dt:Rect', { y: 100 }),
          ]
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:dt=\"http://datatypes\" xmlns:da="http://datatypes-aliased">' +
          '<dt:root>' +
            '<otherBounds xmi:type="da:tRect" z="200" />' +
            '<otherBounds xmi:type="dt:Rect" y="100" />' +
          '</dt:root>' +
          '</xmi:XMI>');
      });


      it('via xsi:type / body property', function() {

        var propertiesModel = createModel([ 'properties' ]);

        // given
        var writer = createWriter(propertiesModel);

        var body = propertiesModel.create('props:SimpleBody', {
          body: '${ foo < bar }'
        });
        var root = xmiCreateRoot(propertiesModel, 'props:WithBody', {
          someBody: body
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
          '<props:withBody>' +
            '<someBody xmi:type="props:SimpleBody">' +
              '${ foo &lt; bar }' +
            '</someBody>' +
          '</props:withBody>' +
          '</xmi:XMI>');
      });


      it('via xsi:type / body property / formated', function() {

        var propertiesModel = createModel([ 'properties' ]);

        // given
        var writer = createWriter(propertiesModel, { format: true });

        var body = propertiesModel.create('props:SimpleBody', { body: '${ foo < bar }' });
        var root = xmiCreateRoot(propertiesModel, 'props:WithBody', { someBody: body });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">\n' +
          '  <props:withBody>\n' +
          '    <someBody xmi:type="props:SimpleBody">${ foo &lt; bar }</someBody>\n' +
          '  </props:withBody>\n' +
          '</xmi:XMI>\n');
      });


      it('keep empty tag', function() {

        // given
        var replaceModel = createModel([ 'replace' ]);

        var writer = createWriter(replaceModel);

        var simple = xmiCreateRoot(replaceModel, 'r:Extension',{ value: '' });

        // when
        var xml = writer.toXML(simple);

        var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:r="http://replace">' +
        '<r:Extension>' +
          '<value></value>' +
        '</r:Extension>' +
        '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });

    });


    describe('attributes', function() {

      it('with line breaks', function() {

        // given
        var model = createModel([ 'properties' ]);

        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:BaseWithId', {
          id: 'FOO\nBAR'
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:baseWithId xmi:id="FOO&#10;BAR" id="FOO&#10;BAR" /></xmi:XMI>');
      });


      it('inherited', function() {

        // given
        var extendedModel = createModel([ 'properties', 'properties-extended' ]);

        var writer = createWriter(extendedModel);

        var root = xmiCreateRoot(extendedModel, 'ext:Root', {
          id: 'FOO'
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:ext="http://extended"><ext:root xmi:id="FOO" id="FOO" /></xmi:XMI>');
      });


      it('extended', function() {

        // given
        var extendedModel = createModel([ 'extension/base', 'extension/custom' ]);

        var writer = createWriter(extendedModel);

        var root = xmiCreateRoot(extendedModel, 'b:SubRoot', {
          customAttr: 1,
          subAttr: 'FOO',
          ownAttr: 'OWN'
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:b="http://base" xmlns:c="http://custom">' +
          '<b:SubRoot ' +
                     'ownAttr="OWN" ' +
                     'c:customAttr="1" ' +
                     'subAttr="FOO" />' +
          '</xmi:XMI>'
        );
      });


      it('ignore undefined attribute values', function() {

        // given
        var model = createModel([ 'properties' ]);

        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:BaseWithId', {
          id: undefined
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:baseWithId /></xmi:XMI>');
      });


      it('ignore null attribute values', function() {

        // given
        var model = createModel([ 'properties' ]);

        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:BaseWithId', {
          id: null
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:baseWithId /></xmi:XMI>');
      });

    });


    describe('simple properties', function() {

      var model = createModel([ 'properties' ]);

      it('attribute', function() {

        // given
        var writer = createWriter(model);

        var attributes = xmiCreateRoot(model, 'props:Attributes', { integerValue: 1000 });

        // when
        var xml = writer.toXML(attributes);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:attributes integerValue="1000" /></xmi:XMI>');
      });


      it('attribute, escaping special characters', function() {

        // given
        var writer = createWriter(model);

        var complex = xmiCreateRoot(model, 'props:Complex', { id: '<>\n&' });

        // when
        var xml = writer.toXML(complex);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:complex xmi:id="&#60;&#62;&#10;&#38;" id="&#60;&#62;&#10;&#38;" /></xmi:XMI>');
      });


      it('write integer property', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:SimpleBodyProperties', {
          intValue: 5
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
          '<props:simpleBodyProperties>' +
            '<intValue>5</intValue>' +
          '</props:simpleBodyProperties>' +
          '</xmi:XMI>';


        // then
        expect(xml).to.eql(expectedXml);
      });


      it('write boolean property', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:SimpleBodyProperties', {
          boolValue: false
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:simpleBodyProperties>' +
            '<boolValue>false</boolValue>' +
          '</props:simpleBodyProperties>' +
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('write boolean property, formated', function() {

        // given
        var writer = createWriter(model, { format: true });

        var root = xmiCreateRoot(model, 'props:SimpleBodyProperties', {
          boolValue: false
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">\n'+
          '  <props:simpleBodyProperties>\n' +
          '    <boolValue>false</boolValue>\n' +
          '  </props:simpleBodyProperties>\n' +
          '</xmi:XMI>\n';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('write string isMany property', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:SimpleBodyProperties', {
          str: [ 'A', 'B', 'C' ]
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:simpleBodyProperties>' +
            '<str>A</str>' +
            '<str>B</str>' +
            '<str>C</str>' +
          '</props:simpleBodyProperties>' +
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('write string isMany property, formated', function() {

        // given
        var writer = createWriter(model, { format: true });

        var root = xmiCreateRoot(model, 'props:SimpleBodyProperties', {
          str: [ 'A', 'B', 'C' ]
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">\n'+
          '  <props:simpleBodyProperties>\n' +
          '    <str>A</str>\n' +
          '    <str>B</str>\n' +
          '    <str>C</str>\n' +
          '  </props:simpleBodyProperties>\n' +
          '</xmi:XMI>\n';

        // then
        expect(xml).to.eql(expectedXml);
      });

    });


    describe('embedded properties', function() {

      var model = createModel([ 'properties' ]);

      var extendedModel = createModel([ 'properties', 'properties-extended' ]);

      it('single', function() {

        // given
        var writer = createWriter(model);

        var complexCount = model.create('props:ComplexCount', { id: 'ComplexCount_1' });
        var embedding = xmiCreateRoot(model, 'props:Embedding', { embeddedComplex: complexCount });

        // when
        var xml = writer.toXML(embedding);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
            '<props:embedding>' +
              '<embeddedComplex xmi:type="props:ComplexCount" xmi:id="ComplexCount_1" id="ComplexCount_1" />' +
            '</props:embedding>' + 
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('property name', function() {

        // given
        var writer = createWriter(model);

        var propertyValue = model.create('props:BaseWithId', { id: 'PropertyValue' });
        var container = xmiCreateRoot(model, 'props:WithProperty', { propertyName: propertyValue });

        // when
        var xml = writer.toXML(container);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
            '<props:withProperty>' +
              '<propertyName xmi:type="props:BaseWithId" xmi:id="PropertyValue" id="PropertyValue" />' +
            '</props:withProperty>'+
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('collection', function() {

        // given
        var writer = createWriter(model);

        var attributes = model.create('props:Attributes', { id: 'Attributes_1' });
        var simpleBody = model.create('props:SimpleBody');
        var containedCollection = model.create('props:ContainedCollection');

        var root = xmiCreateRoot(model, 'props:Root', {
          any: [
            attributes,
            simpleBody,
            containedCollection,
          ]
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
          '<props:root>' +
            '<any xmi:type="props:Attributes" xmi:id="Attributes_1" id="Attributes_1" />' +
            '<any xmi:type="props:SimpleBody" />' +
            '<any xmi:type="props:ContainedCollection" />' +
          '</props:root></xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('collection / different ns', function() {

        // given
        var writer = createWriter(extendedModel);

        
        var attributes1 = extendedModel.create('props:Attributes', { id: 'Attributes_1' });
        var attributes2 = extendedModel.create('props:Attributes', { id: 'Attributes_2' });
        var extendedComplex = extendedModel.create('ext:ExtendedComplex', { numCount: 100 });
        
        var root = xmiCreateRoot(extendedModel, "ext:Root", {
          any: [
            attributes1,
            attributes2,
            extendedComplex
          ],
          elements: [extendedModel.create('ext:Base')]
        });
        
        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:ext="http://extended" xmlns:props="http://properties">' +
            '<ext:root>' +
              '<any xmi:type="props:Attributes" xmi:id="Attributes_1" id="Attributes_1" />' +
              '<any xmi:type="props:Attributes" xmi:id="Attributes_2" id="Attributes_2" />' +
              '<any xmi:type="ext:ExtendedComplex" numCount="100" />' +
              '<elements xmi:type="ext:Base" />' +
            '</ext:root>' + 
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });

  //   });


    describe('virtual properties', function() {

      var model = createModel([ 'virtual' ]);

      it('should not serialize virtual property', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'virt:Root', {
          child: model.create('virt:Child')
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:virt="http://virtual">' +
            '<virt:Root />'+
          '</xmi:XMI>'
        );
      });

    });


    describe('body text', function() {

      var model = createModel([ 'properties' ]);

      it('write body text property', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:SimpleBody', {
          body: 'textContent'
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:simpleBody>textContent</props:simpleBody></xmi:XMI>');
      });


      it('write encode body property', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:SimpleBody', {
          body: '<h2>HTML&nbsp;"markup"</h2>'
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
          '<props:simpleBody>' +
            '&lt;h2&gt;HTML&amp;nbsp;"markup"&lt;/h2&gt;' +
          '</props:simpleBody></xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('write encode body property in subsequent calls', function() {

        // given
        var writer = createWriter(model);

        var root1 = xmiCreateRoot(model, 'props:SimpleBody', {
          body: '<>'
        });
        var root2 = xmiCreateRoot(model, 'props:SimpleBody', {
          body: '<>'
        });

        // when
        var xml1 = writer.toXML(root1);
        var xml2 = writer.toXML(root2);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
          '<props:simpleBody>' +
            '&lt;&gt;' +
          '</props:simpleBody></xmi:XMI>';

        // then
        expect(xml1).to.eql(expectedXml);
        expect(xml2).to.eql(expectedXml);
      });


      it('write encode body property with special chars', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:SimpleBody', {
          body: '&\n<>"\''
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
          '<props:simpleBody>' +
            '&amp;\n&lt;&gt;"\'' +
          '</props:simpleBody>' +
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });

    });


    describe('alias', function() {

      var model = createModel([ 'properties' ]);

      var noAliasModel = createModel(['noalias']);

      it('should lowerCase', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:Root');

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:root /></xmi:XMI>');
      });


      it('none', function() {

        // given
        var writer = createWriter(noAliasModel);

        var root = xmiCreateRoot(noAliasModel, 'na:Root');

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:na="http://noalias"><na:Root /></xmi:XMI>');
      });
    });


    describe('ns', function() {

      var model = createModel([ 'properties' ]);
      var extendedModel = createModel([ 'properties', 'properties-extended' ]);

      it('single package', function() {

        // given
        var writer = createWriter(model);

        var root = xmiCreateRoot(model, 'props:Root');

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:root /></xmi:XMI>');
      });


      it('multiple packages', function() {

        // given
        var writer = createWriter(extendedModel);

        var root = xmiCreateRoot(extendedModel, 'props:Root', {
          any: [extendedModel.create('ext:ExtendedComplex')]
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties" xmlns:ext="http://extended">'+ 
            '<props:root>' +
              '<any xmi:type="ext:ExtendedComplex" />' +
            '</props:root>'+
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('default ns', function() {

        // given
        var writer = createWriter(extendedModel);

        var root = xmiCreateRoot(extendedModel, 'props:Root', {}, { xmlns: 'http://properties' });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns="http://properties" xmlns:xmi="http://www.omg.org/spec/XMI/20131001"><root /></xmi:XMI>');
      });


      it('default ns / attributes', function() {

        // given
        var writer = createWriter(extendedModel);

        var root = xmiCreateRoot(extendedModel, 'props:Root', { 
            id: 'Root',
            any: [
              extendedModel.create('ext:ExtendedComplex'),
              extendedModel.create('props:Attributes', { id: 'Attributes_2' }),
            ]
          }, {
            xmlns: 'http://properties'
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml)
          .to.eql('<xmi:XMI xmlns="http://properties" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:ext="http://extended"><root xmi:id="Root" id="Root">' +
                     '<any xmi:type="ext:ExtendedComplex" />' +
                     '<any xmi:type="Attributes" xmi:id="Attributes_2" id="Attributes_2" />' +
                   '</root></xmi:XMI>');
      });


      it('default ns / extension attributes', function() {

        // given
        var writer = createWriter(extendedModel);

        var root = xmiCreateRoot(extendedModel, 'props:Root', {
          id: 'Root',
          'foo:bar': 'BAR'
        }, {
          xmlns: 'http://properties',
          'xmlns:foo': 'http://fooo',
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns="http://properties" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:foo="http://fooo"><root xmi:id="Root" id="Root" foo:bar="BAR" /></xmi:XMI>');
      });


      it('explicit ns / attributes', function() {

        // given
        var writer = createWriter(extendedModel);

        var root = xmiCreateRoot(extendedModel, 'props:Root', {
            id: 'Root' 
          }, {
            'xmlns:foo': 'http://properties'
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:foo="http://properties" xmlns:xmi="http://www.omg.org/spec/XMI/20131001"><foo:root xmi:id="Root" id="Root" /></xmi:XMI>');
      });

    });


    describe('reference', function() {

      var model = createModel([ 'properties' ]);

      it('single', function() {

        // given
        var writer = createWriter(model);

        var complex = model.create('props:Complex', { id: 'Complex_1' });
        var referencingSingle = xmiCreateRoot(model, 'props:ReferencingSingle', { referencedComplex: complex });

        // when
        var xml = writer.toXML(referencingSingle);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:referencingSingle referencedComplex="Complex_1" /></xmi:XMI>');
      });


      it('collection', function() {

        // given
        var writer = createWriter(model);

        var complexCount = model.create('props:ComplexCount', { id: 'ComplexCount_1' });
        var complexNesting = model.create('props:ComplexNesting', { id: 'ComplexNesting_1' });

        var referencingCollection = xmiCreateRoot(model, 'props:ReferencingCollection', {
          references: [ complexCount, complexNesting ]
        });

        // when
        var xml = writer.toXML(referencingCollection);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties">' +
            '<props:referencingCollection>' +
              '<references>ComplexCount_1</references>' +
              '<references>ComplexNesting_1</references>' +
            '</props:referencingCollection>' +
          '</xmi:XMI>'
        );
      });


      it('attribute collection', function() {

        // given
        var writer = createWriter(model);

        var complexCount = model.create('props:ComplexCount', { id: 'ComplexCount_1' });
        var complexNesting = model.create('props:ComplexNesting', { id: 'ComplexNesting_1' });

        var attrReferenceCollection = xmiCreateRoot(model, 'props:AttributeReferenceCollection', {
          refs: [ complexCount, complexNesting ]
        });

        // when
        var xml = writer.toXML(attrReferenceCollection);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:props="http://properties"><props:attributeReferenceCollection refs="ComplexCount_1 ComplexNesting_1" /></xmi:XMI>');
      });

    });


    it('redefined properties', function() {

      // given
      var model = createModel([ 'redefine' ]);

      var writer = createWriter(model);

      var element = xmiCreateRoot(model, 'r:Extension', {
        id: 1,
        name: 'FOO',
        value: 'BAR'
      });

      var expectedXml = 
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:r="http://redefine">' +
          '<r:Extension>' +
            '<id>1</id>' +
            '<name>FOO</name>' +
            '<value>BAR</value>' +
          '</r:Extension>'+
        '</xmi:XMI>';

      // when
      var xml = writer.toXML(element);

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('replaced properties', function() {

      // given
      var model = createModel([ 'replace' ]);

      var writer = createWriter(model);

      var element = xmiCreateRoot(model, 'r:Extension', {
        id: 1,
        name: 'FOO',
        value: 'BAR'
      });

      var expectedXml = 
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:r="http://replace">' +
          '<r:Extension>' +
            '<name>FOO</name>' +
            '<value>BAR</value>' +
            '<id>1</id>' +
          '</r:Extension>' + 
        '</xmi:XMI>';

      // when
      var xml = writer.toXML(element);

      // then
      expect(xml).to.eql(expectedXml);
    });

  });


  describe('extension handling', function() {

    var extensionModel = createModel([ 'extensions' ]);


    describe('attributes', function() {

      it('should write xsi:schemaLocation', function() {

        // given
        var writer = createWriter(extensionModel);

        var root = xmiCreateRoot(extensionModel, 'e:Root', {
          'xsi:schemaLocation': 'http://fooo ./foo.xsd'
        });

        // when
        var xml = writer.toXML(root);

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><e:root ' +
                  '' +
                  'xsi:schemaLocation="http://fooo ./foo.xsd" /></xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('should write extension attributes', function() {

        // given
        var writer = createWriter(extensionModel);

        var root = xmiCreateRoot(extensionModel, 'e:Root', {
          'foo:bar': 'BAR'
        },{
          'xmlns:foo': 'http://fooo',
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions" xmlns:foo="http://fooo"><e:root foo:bar="BAR" /></xmi:XMI>');
      });

    });


    describe('elements', function() {

      it('should write self-closing extension elements', function() {

        // given
        var writer = createWriter(extensionModel);

        var meta1 = extensionModel.createAny('other:meta', 'http://other', {
          key: 'FOO',
          value: 'BAR'
        });

        var meta2 = extensionModel.createAny('other:meta', 'http://other', {
          key: 'BAZ',
          value: 'FOOBAR'
        });

        var root = xmiCreateRoot(extensionModel, 'e:Root', {
          id: 'FOO',
          extensions: [ meta1, meta2 ]
        });

        // when
        var xml = writer.toXML(root);

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions" xmlns:other="http://other">'+
            '<e:root xmi:id="FOO">' +
              '<id>FOO</id>' +
              '<extensions xmi:type="other:meta" key="FOO" value="BAR" />' +
              '<extensions xmi:type="other:meta" key="BAZ" value="FOOBAR" />' +
            '</e:root>'+
          '</xmi:XMI>'
        );
      });


      // #23
      it('should write unqualified element', function() {

        // given
        var writer = createWriter(extensionModel);

        // explicitly create element with elementForm=unqualified
        var root = extensionModel.createAny('root', undefined, {
          key: 'FOO',
          value: 'BAR'
        });

        // when
        var xml = writer.toXML(xmiWrap(extensionModel, root));

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001"><root key="FOO" value="BAR" /></xmi:XMI>'
        );
      });


      it('should write extension element body', function() {

        // given
        var writer = createWriter(extensionModel);

        var note = extensionModel.createAny('other:note', 'http://other', {
          $body: 'a note'
        });

        var root = extensionModel.create('e:Root', {
          id: 'FOO',
          extensions: [ note ]
        });

        // when
        var xml = writer.toXML(xmiWrap(extensionModel, root));

        // then
        expect(xml).to.eql(
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions" xmlns:other="http://other">'+
            '<e:root xmi:id="FOO">' +
              '<id>FOO</id>' +
              '<extensions xmi:type="other:note">' +
                'a note' +
              '</extensions>' +
            '</e:root>'+
          '</xmi:XMI>'
          );
      });


      // it('should write nested extension element', function() {

      //   // given
      //   var writer = createWriter(extensionModel);

      //   var meta1 = extensionModel.createAny('other:meta', 'http://other', {
      //     key: 'k1',
      //     value: 'v1'
      //   });

      //   var meta2 = extensionModel.createAny('other:meta', 'http://other', {
      //     key: 'k2',
      //     value: 'v2'
      //   });

      //   var additionalNote = extensionModel.createAny('other:additionalNote', 'http://other', {
      //     $body: 'this is some text'
      //   });

      //   var nestedMeta = extensionModel.createAny('other:nestedMeta', 'http://other', {
      //     $children: [ meta1, meta2, additionalNote ]
      //   });

      //   var root = extensionModel.create('e:Root', {
      //     id: 'FOO',
      //     extensions: [ nestedMeta ]
      //   });

      //   // when
      //   var xml = writer.toXML(xmiWrap(extensionModel, root));

      //   var expectedXml =
      //     '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions" xmlns:other="http://other">' +
      //       '<e:root xmi:id="FOO">' +
      //         '<id>FOO</id>' + 
      //         '<extensions xmi:type="other:nestedMeta">' + //xmi:type="other:nestedMeta"
      //           '<other:meta key="k1" value="v1" />' +
      //           '<other:meta key="k2" value="v2" />' +
      //           '<other:additionalNote>' +
      //             'this is some text' +
      //           '</other:additionalNote>' +
      //         '</extensions>' +
      //       '</e:root>'+
      //     '</xmi:XMI>';

      //   // then
      //   expect(xml).to.eql(expectedXml);
      // });
    });

  });


  describe('qualified extensions', function() {

    var extensionModel = createModel([ 'extension/base', 'extension/custom' ]);


    it('should write typed extension property', function() {

      // given
      var writer = createWriter(extensionModel);

      var customGeneric = extensionModel.create('c:CustomGeneric', { count: 10 });

      var root = extensionModel.create('b:Root', {
        generic: customGeneric
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:b="http://base" xmlns:c="http://custom">' +
          '<b:Root>' +
            '<generic xmi:type="c:CustomGeneric" count="10" />' +
          '</b:Root>'+
        '</xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('should write typed extension attribute', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('b:Root', { customAttr: 666 });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:b="http://base" xmlns:c="http://custom"><b:Root c:customAttr="666" /></xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('should write generic collection', function() {

      // given
      var writer = createWriter(extensionModel);

      var property1 = extensionModel.create('c:Property', { key: 'foo', value: 'FOO' });
      var property2 = extensionModel.create('c:Property', { key: 'bar', value: 'BAR' });

      var any = extensionModel.createAny('other:Xyz', 'http://other', {
        $body: 'content'
      });

      var root = extensionModel.create('b:Root', {
        genericCollection: [ property1, property2, any ]
      });

      var xml = writer.toXML(xmiWrap(extensionModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:b="http://base" xmlns:c="http://custom" xmlns:other="http://other">' + // xmlns:other="http://other"
          '<b:Root>' +
            '<genericCollection xmi:type="c:Property" key="foo" value="FOO" />' +
            '<genericCollection xmi:type="c:Property" key="bar" value="BAR" />' +
            '<genericCollection xmi:type="other:Xyz">content</genericCollection>' +
          '</b:Root>'+
        '</xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);

    });

  });


  describe('namespace declarations', function() {

    var extensionModel = createModel([ 'extensions' ]);

    var extendedModel = createModel([
      'properties',
      'properties-extended'
    ]);


    describe('should deconflict namespace prefixes', function() {

      it('on nested Any', function() {

        // given
        var writer = createWriter(extensionModel);

        var root = extensionModel.create('e:Root', {
          extensions: [
            extensionModel.createAny('e:foo', 'http://not-extensions', {
              foo: 'BAR'
            })
          ]
        });

        // when
        var xml = writer.toXML(xmiWrap(extensionModel, root));

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions" xmlns:e_1="http://not-extensions">' +
            '<e:root>' +
              '<extensions xmi:type="e_1:foo" foo="BAR" />' +
            '</e:root>'+
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('on explicitly added namespace', function() {

        // given
        var writer = createWriter(extensionModel);

        var root = extensionModel.create('e:Root');

        // when
        var xml = writer.toXML(xmiWrap(extensionModel, root, {
          'xmlns:e': 'http://not-extensions'
        }));

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e_1="http://extensions" xmlns:e="http://not-extensions">' +
            '<e_1:root />'+
          '</xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });


      it('on explicitly added namespace + Any', function() {

        // given
        var writer = createWriter(extensionModel);

        var root = extensionModel.create('e:Root', {
          extensions: [
            extensionModel.createAny('e:foo', 'http://not-extensions', {
              foo: 'BAR'
            })
          ]
        });

        // when
        var xml = writer.toXML(xmiWrap(extensionModel, root, {
          'xmlns:e': 'http://not-extensions',
        }));

        var expectedXml =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e_1="http://extensions" xmlns:e="http://not-extensions">' +
          '<e_1:root>' +
            '<extensions xmi:type="e:foo" foo="BAR" />' +
          '</e_1:root></xmi:XMI>';

        // then
        expect(xml).to.eql(expectedXml);
      });

    });


    it('should write manually added custom namespace', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('e:Root');

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root, {
        'xmlns:foo': 'http://fooo'
      }));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions" xmlns:foo="http://fooo">'+
          '<e:root />'+
          '</xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('should ignore unknown namespace prefix', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('e:Root', {
        'foo:bar': 'BAR'
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      // then
      expect(xml).to.eql('<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions"><e:root /></xmi:XMI>');
    });


    it('should write custom', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('e:Root', {

        // unprefixed root namespace
        extensions: [
          extensionModel.createAny('bar:bar', 'http://bar', {
            'xmlns:bar': 'http://bar',
            $children: [
              extensionModel.createAny('other:child', 'http://other', {
                'xmlns:other': 'http://other',
                b: 'B'
              })
            ]
          }),
          extensionModel.createAny('ns0:foo', 'http://foo', {

            // unprefixed extension namespace
            'xmlns': 'http://foo',
            $children: [
              extensionModel.createAny('ns0:child', 'http://foo', {
                a: 'A'
              })
            ]
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root, {
        'xmlns': 'http://extensions',
      }));

      var expectedXml =
        '<xmi:XMI xmlns="http://extensions" xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root>' +
          '<extensions xmlns:bar="http://bar" xmi:type="bar:bar">' +
            '<other:child xmlns:other="http://other" b="B" />' +
          '</extensions>' +
          '<extensions xmlns="http://foo" xmi:type="foo">' +
            '<child a="A" />' +
          '</extensions>' +
        '</root></xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);

    });


    it('should write nested custom', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('e:Root', {

        // unprefixed root namespace
        'xmlns': 'http://extensions',
        extensions: [
          extensionModel.createAny('bar:bar', 'http://bar', {
            'xmlns:bar': 'http://bar',
            'bar:attr': 'ATTR'
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root xmlns="http://extensions">' +
          '<extensions xmlns:bar="http://bar" xmi:type="bar:bar" attr="ATTR" />' +
        '</root></xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('should strip redundant nested custom', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('e:Root', {

        // unprefixed root namespace
        'xmlns': 'http://extensions',
        'xmlns:bar': 'http://bar',
        extensions: [
          extensionModel.createAny('bar:bar', 'http://bar', {
            'xmlns:bar': 'http://bar',
            'bar:attr': 'ATTR'
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root xmlns="http://extensions" xmlns:bar="http://bar">' +
          '<extensions xmi:type="bar:bar" attr="ATTR" />' +
        '</root></xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('should strip different prefix nested custom', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('e:Root', {

        // unprefixed root namespace
        'xmlns': 'http://extensions',
        'xmlns:otherBar': 'http://bar',
        'xmlns:otherFoo': 'http://foo',
        extensions: [
          extensionModel.createAny('bar:bar', 'http://bar', {
            'xmlns:bar': 'http://bar',
            'xmlns:foo': 'http://foo',
            'bar:attr': 'ATTR',
            'foo:attr': 'FOO_ATTR'
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root xmlns="http://extensions" xmlns:otherBar="http://bar" ' +
              'xmlns:otherFoo="http://foo">' +
          '<extensions xmi:type="otherBar:bar" attr="ATTR" otherFoo:attr="FOO_ATTR" />' +
        '</root></xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('should write normalized custom', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('e:Root', {

        // unprefixed root namespace
        'xmlns': 'http://extensions',
        'xmlns:otherBar': 'http://bar',
        extensions: [
          extensionModel.createAny('bar:bar', 'http://bar', {
            'bar:attr': 'ATTR'
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root xmlns="http://extensions" xmlns:otherBar="http://bar">' +
          '<extensions xmi:type="otherBar:bar" attr="ATTR" />' +
        '</root></xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('should write wellknown', function() {

      // given
      var writer = createWriter(extendedModel);

      var root = extendedModel.create('props:Root', {

        // unprefixed top-level namespace
        'xmlns': 'http://properties',
        any: [
          extendedModel.create('ext:ExtendedComplex', {

            // unprefixed nested namespace
            'xmlns': 'http://extended'
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root xmlns="http://properties">' +
          '<any xmlns="http://extended" xmi:type="ExtendedComplex" />' +
        '</root></xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);
    });


    it('should write only actually exposed', function() {

      // given
      var writer = createWriter(extendedModel);

      var root = extendedModel.create('ext:Root', {

        // unprefixed top-level namespace
        'xmlns': 'http://extended',
        id: 'ROOT',
        any: [
          extendedModel.create('props:Complex', {

            // unprefixed nested namespace
            'xmlns': 'http://properties'
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extendedModel, root));

      var expectedXml =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root xmlns="http://extended" xmi:id="ROOT" id="ROOT">' +
          '<any xmlns="http://properties" xmi:type="Complex" />' +
        '</root></xmi:XMI>';

      // then
      expect(xml).to.eql(expectedXml);

    });


    it('should write xsi:type namespaces', function() {

      var model = createModel([
        'datatype',
        'datatype-external',
        'datatype-aliased'
      ]);

      // given
      var writer = createWriter(model);

      var root = model.create('da:Root', {
        'xmlns:a' : 'http://datatypes-aliased',
        otherBounds: [
          model.create('dt:Rect', {
            'xmlns': 'http://datatypes',
            y: 100
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(model, root));

      // then
      expect(xml).to.eql(
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<a:Root xmlns:a="http://datatypes-aliased">' +
          '<otherBounds xmlns="http://datatypes" xmi:type="Rect" y="100" />' +
        '</a:Root></xmi:XMI>');

    });


    it('should strip unused global', function() {

      // given
      var writer = createWriter(extendedModel);

      var root = extendedModel.create('ext:Root', {
        xmlns: 'http://extended',
        id: 'Root',
        'xmlns:props': 'http://properties',
        any: [
          extendedModel.create('props:Base', { xmlns: 'http://properties' })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extendedModel, root));

      // then
      expect(xml).to.eql(
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root xmlns="http://extended" xmi:id="Root" id="Root">' +
          '<any xmlns="http://properties" xmi:type="Base" />' +
        '</root></xmi:XMI>'
      );
    });


    it('should strip xml namespace', function() {

      // given
      var writer = createWriter(extensionModel);

      var root = extensionModel.create('e:Root', {
        'xml:lang': 'de',
        extensions: [
          extensionModel.createAny('bar:bar', 'http://bar', {
            'xml:lang': 'en'
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extensionModel, root));

      // then
      expect(xml).to.eql(
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:e="http://extensions" xmlns:bar="http://bar">'+
          '<e:root xml:lang="de">' +
            '<extensions xmi:type="bar:bar" xml:lang="en" />' +
          '</e:root>'+
        '</xmi:XMI>'
      );
    });


    it('should keep local override', function() {

      // given
      var writer = createWriter(extendedModel);

      var root = extendedModel.create('props:ComplexNesting', {
        'xmlns:root': 'http://properties',
        id: 'ComplexNesting',
        nested: [
          extendedModel.create('props:ComplexNesting', {
            xmlns: 'http://properties',
            nested: [
              extendedModel.create('props:ComplexNesting', {
                nested: [
                  extendedModel.create('props:ComplexNesting', {
                    'xmlns:foo': 'http://properties'
                  })
                ]
              })
            ]
          })
        ]
      });

      // when
      var xml = writer.toXML(xmiWrap(extendedModel, root));

      // then
      expect(xml).to.eql(
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
        '<root:complexNesting xmlns:root="http://properties" xmi:id="ComplexNesting" id="ComplexNesting">' +
          '<nested xmlns="http://properties" xmi:type="ComplexNesting">' +
            '<nested xmi:type="ComplexNesting">' +
              '<nested xmlns:foo="http://properties" xmi:type="foo:ComplexNesting" />' +
            '</nested>' +
          '</nested>' +
        '</root:complexNesting>' +
        '</xmi:XMI>'
      );
    });

  });


  it('should reuse global namespace', function() {

    var model = createModel([
      'properties',
      'properties-extended'
    ]);

    // given
    var writer = createWriter(model);

    // <props:Root xmlns:props="http://properties" xmlns:ext="http://extended">
    //   <complexNesting... xmlns="http://props">
    //     <ext:extendedComplex numCount="1" />
    //   </complexNesting>
    // </props:root>

    var root = xmiCreateRoot(model, 'props:Root', {
      'xmlns:props': 'http://properties',
      'xmlns:ext': 'http://extended',
      any: [
        model.create('props:ComplexNesting', {
          'xmlns': 'http://properties',
          nested: [
            model.create('ext:ExtendedComplex', { numCount: 1 })
          ]
        })
      ]
    });

    // when
    var xml = writer.toXML(root);

    var expectedXML =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001">'+
          '<props:root xmlns:props="http://properties" xmlns:ext="http://extended">' +
            '<any xmlns="http://properties" xmi:type="ComplexNesting">' +
              '<nested xmi:type="ext:ExtendedComplex" numCount="1" />' +
            '</any>' +
          '</props:root>'+
      '</xmi:XMI>';

    // then
    expect(xml).to.eql(expectedXML);

  });

  });

  describe('linking', function () {

    it('should link to type', function() {
      var model = createModel(['domainconcept', 'todo', 'uml']);

      // given
      var writer = createWriter(model);
      var type = model.getType("todo:Todo", { id: "X" });

      var root = model.create('dc:Domainconcept', {
          classifier: type.$descriptor
      });
      // when
      var xml = writer.toXML(xmiWrap(model, root));
  
      var expectedXML =
          '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:dc="http://moddletest/domainconcept" xmlns:todo="http://moddletest/todo"><dc:Domainconcept classifier="todo:Todo" /></xmi:XMI>';
  
      // then
      expect(xml).to.eql(expectedXML);
    });
  });
 
  it('should not use external ns in inherited attributes uml', function() {
    var model = createModel(['domainconcept', 'todo', 'uml']);

    // given
    var writer = createWriter(model);
    var type = model.getType("todo:Todo", { id: "X" });

    var root = model.create('todo:Todo', {
      name: "myTodo"
  });
    // when
    var xml = writer.toXML(xmiWrap(model, root));

    var expectedXML =
        '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:todo="http://moddletest/todo"><todo:Todo name="myTodo" /></xmi:XMI>';

    // then
    expect(xml).to.eql(expectedXML);
  });

  it('should not use external ns in inherited attributes', function() {

    var model = createModel([
      'properties',
      'properties-extended'
    ]);

    // given
    var writer = createWriter(model);

    var root = 
        model.create('ext:ExtendedComplex', {
          'id': 'id',
          'count': 1,
          'count2': 2,
          'someattr': "test"
        });

    // when
    var xml = writer.toXML(xmiWrap(model, root));

    var expectedXML =
      '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" xmlns:ext="http://extended">' +
          '<ext:extendedComplex xmi:id="id" id="id" someattr="test" numCount="1" count2="2" />' +
      '</xmi:XMI>';

    // then
    expect(xml).to.eql(expectedXML);

  });
});
