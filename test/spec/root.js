import expect from '../expect.js';

import {
  Reader
} from '../../lib/index.js';

import {
  readFile,
  createXMIModelBuilder
} from '../helper.js';


describe('Root Element', function() {
  var createModel = createXMIModelBuilder('test/fixtures/model/');
  var model = createModel([ 'properties' ]);
  
  it('simple xmi self closing', async function() {

    var xml = '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" />';

    var reader = new Reader(model);
    var rootHandler = reader.handler();
    // when
    var ret = await reader.fromXML(xml, rootHandler);

    // then
    expect(ret.warnings).to.have.lengthOf(0);
  });
  it('simple xmi self closing', async function() {

    var xml = '<xmi:XMI xmlns:xmi="http://www.omg.org/spec/XMI/20131001" />';

    var reader = new Reader(model);
    var rootHandler = reader.handler();
    // when
    var ret = await reader.fromXML(xml, rootHandler);

    // then
    expect(ret.warnings).to.have.lengthOf(0);
  });

  it('simple xmi', async function() {

    var xml = '<xmi:XMI xmlns:props="http://properties" xmlns:xmi="http://www.omg.org/spec/XMI/20131001"></xmi:XMI>';

    var reader = new Reader(model);
    var rootHandler = reader.handler();

    // when
    var ret = await reader.fromXML(xml, rootHandler);

    // then
    expect(ret.warnings).to.have.lengthOf(0);
  });

  it('invalid type type', async function() {

    var xml = '<props:referencingCollection2 xmlns:props="http://properties" id="C_4">' +
                '<props:references>C_2</props:references>' +
                '<props:references>C_5</props:references>' +
              '</props:referencingCollection2>';

    var reader = new Reader(model);
    var rootHandler = reader.handler();

    var expectedError =
      'unparsable content <props:referencingCollection2> detected\n\t' +
          'line: 0\n\t' +
          'column: 0\n\t' +
          'nested error: unexpected element <props:referencingCollection2>';

    var err;

    // when
    try {
      await reader.fromXML(xml, rootHandler);
    } catch (_err) {
      err = _err;
    }

    // then
    expect(err).to.exist;
    expect(err.message).to.eql(expectedError);
  });

  it('invalid type type for xmi', async function() {

    var xml = '<props:referencingCollection2 xmlns:props="http://properties" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" id="C_4">' +
                '<props:references>C_2</props:references>' +
                '<props:references>C_5</props:references>' +
              '</props:referencingCollection2>';

    var reader = new Reader(model);
    var rootHandler = reader.handler();
    var expectedError =
      'unparsable content <props:referencingCollection2> detected\n\t' +
          'line: 0\n\t' +
          'column: 0\n\t' +
          'nested error: unexpected element <props:referencingCollection2>';

    var err;

    // when
    try {
      await reader.fromXML(xml, rootHandler);
    } catch (_err) {
      err = _err;
    }

    // then
    expect(err).to.exist;
    expect(err.message).to.eql(expectedError);
  });

  it('invalid type type for xmi without child', async function() {

    var xml = '<props:referencingCollection2 xmlns:props="http://properties" xmlns:xmi="http://www.omg.org/spec/XMI/20131001" id="C_4" />';

    var reader = new Reader(model);
    var rootHandler = reader.handler();
    var expectedError =
      'unparsable content <props:referencingCollection2> detected\n\t' +
          'line: 0\n\t' +
          'column: 0\n\t' +
          'nested error: unexpected element <props:referencingCollection2>';

    var err;

    // when
    try {
      await reader.fromXML(xml, rootHandler);
    } catch (_err) {
      err = _err;
    }

    // then
    expect(err).to.exist;
    expect(err.message).to.eql(expectedError);
  });

  it('invalid child', async function() {

    var xml = '<props:ReferencingCollection xmlns:props="http://properties" id="C_4" xmlns:xmi="http://www.omg.org/spec/XMI/20131001">' +
                '<props:references2>C_2</props:references2>' +
              '</props:ReferencingCollection>';

    var reader = new Reader(model);
    var rootHandler = reader.handler();

    var expectedError =
      'unparsable content <props:references2> detected\n\t' +
          'line: 0\n\t' +
          'column: 119\n\t' +
          'nested error: unknown type <props:References2>';

    var err;

    // when
    try {
      await reader.fromXML(xml, rootHandler);
    } catch (_err) {
      err = _err;
    }

    // then
    expect(err).to.exist;
    expect(err.message).to.eql(expectedError);
  });

  
  it('no xmi - wrong type', async function() {

    var xml = '<props:referencingCollection xmlns:props="http://properties" id="C_4">' +
                '<props:references>C_2</props:references>' +
                '<props:references>C_5</props:references>' +
              '</props:referencingCollection>';

    var reader = new Reader(model);
    var rootHandler = reader.handler('props:ComplexAttrs');

    var expectedError =
      'unparsable content <props:referencingCollection> detected\n\t' +
          'line: 0\n\t' +
          'column: 0\n\t' +
          'nested error: unexpected element <props:referencingCollection>';

    var err;

    // when
    try {
      await reader.fromXML(xml, rootHandler);
    } catch (_err) {
      err = _err;
    }

    // then
    expect(err).to.exist;
    expect(err.message).to.eql(expectedError);
  });

});
