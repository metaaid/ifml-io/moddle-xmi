import expect from '../expect.js';

import {
  Reader
} from '../../lib/index.js';

import {
  readFile,
  createXMIModelBuilder
} from '../helper.js';


describe('XMI-Reader', function() {

  var createModel = createXMIModelBuilder('test/fixtures/model/');


  describe('api', function() {

    var model = createModel([ 'di', 'ifml','ifmldi' ]);

    it('model - dia connection', async function() {

      // given
      var reader = new Reader(model);
      var rootHandler = reader.handler();

      var xml = `<?xml version="1.0" encoding="UTF-8"?>
      <xmi:XMI xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:ifml="http://www.omg.org/spec/IFML/20140301"
        xmlns:ifmldi="http://www.omg.org/spec/IFML/20130218/IFML-DI"
        xmlns:dc="http://www.omg.org/spec/DD/20100524/DC"
        xmlns:xmi="http://www.omg.org/spec/XMI/20131001">
        <ifml:IFMLModel>
          <interactionFlowModel xmi:type="ifml:InteractionFlowModel" xmi:id="_myModel">
          </interactionFlowModel>
        </ifml:IFMLModel>
        <ifmldi:IFMLDiagram modelElement="_myModel">
        </ifmldi:IFMLDiagram>
      </xmi:XMI>`;

      // when
      var {
        rootElement,
        elementsById,
        warnings,
        references
      } = await reader.fromXML(xml, rootHandler);
      console.log(elementsById);
      // then
      expect(rootElement).to.exist;
      expect(warnings).to.eql([]);
      expect(elementsById).to.eql({
        '_myModel': rootElement.ownedElement[0].interactionFlowModel
      });
      expect(references).to.eql([{
        element: rootElement.ownedElement[1],
        id: "_myModel",
        property: 'ifmldi:modelElement'
      }]);
    });
  });

});
